﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowDotNetAPI.Models;

namespace BossPrioritizer.Constants
{
    class AppConstants
    {
        public static string RaiderListFile { get { return "RaiderList.txt"; } }
        private static string Key { get { return "gcghfeme2e3f6z693e5q9uum5s9zdyna"; } }
        public static string RaiderAuditFile { get { return "RaiderAudit.txt"; } }
        public static string AMRWeightAddress { get { return "http://www.askmrrobot.com/wod/json/strategies/v175"; } }
        public static string StatWeightTextFilePath { get { return "JsonSample.txt"; } }
        public static string BaseDirPath { get { return "..\\..\\"; } }
        public static string BossDropFile (string raid, string difficulty)
        {
            return raid + " - " + difficulty + ".txt";
        }

        public static List<string> GetRaiders()
        {
            List<String> raiders = new List<string>();
            raiders.Add("Kurston-Kul Tiras");
            raiders.Add("Säm-Bladefist");
            raiders.Add("Chelan-Kul Tiras");
            raiders.Add("Brejwelb-Bladefist");
            raiders.Add("Bynnther-Bladefist");
            raiders.Add("Icesickels-Bladefist");
            raiders.Add("Raarios-Bladefist");
            ////raiders.Add("Retroplay-Bladefist");
            ////raiders.Add("îre-Bladefist");
            raiders.Add("Chepe-Kul Tiras");
            //raiders.Add("Floy-Kul Tiras");
            raiders.Add("Fâlerhard-Kul Tiras");
            //raiders.Add("Parney-Kul Tiras");
            raiders.Add("Thangone-Kul Tiras");
            //raiders.Add("Vurge-Kul Tiras");
            raiders.Add("Waylann-Kul Tiras");
            raiders.Add("Wulfrem-Kul Tiras");
            raiders.Add("Zandialara-Kul Tiras");
            raiders.Add("Syredel-Kul Tiras");
            //raiders.Add("Mindbomb-Kul Tiras");
            return raiders;
        }

        public static string GetKey()
        {
            return BossPrioritizer.Constants.AppConstants.Key;
        }

        public static IEnumerable<string> GetRaidNames()
        {
            List<string> raidNames = new List<string>();
            raidNames.Add("Highmaul");
            raidNames.Add("Blackrock Foundry");
            return raidNames;
        }

        public static IEnumerable<string> GetDifficulties()
        {
            List<string> difficultyNames = new List<string>();
            difficultyNames.Add("normal");
            difficultyNames.Add("heroic");
            difficultyNames.Add("mythic");
            return difficultyNames;
        }

        public static IEnumerable<string> GetBossNames(string raidName)
        {
            List<string> bossNames = new List<string>();
            if (raidName == "Highmaul")
            {
                bossNames.Add("Kargath Bladefist");
                bossNames.Add("The Butcher");
                bossNames.Add("Brackenspore");
                bossNames.Add("Tectus");
                bossNames.Add("Twin Ogron");
                bossNames.Add("Ko'ragh");
                bossNames.Add("Imperator Mar'gok");
            }
            else if (raidName == "Blackrock Foundry")
            {
                bossNames.Add("Gruul");
                bossNames.Add("Oregorger");
                bossNames.Add("Blast Furnace");
                bossNames.Add("Hans'gar and Franzok");
                bossNames.Add("Flamebender Ka'graz");
                bossNames.Add("Kromog");
                bossNames.Add("Beastlord Darmac");
                bossNames.Add("Operator Thogar");
                bossNames.Add("Iron Maidens");
                bossNames.Add("Blackhand");
            }
            return bossNames;
        }

        public static int BossNameToInt(string BossName)
        {
            if ((BossName == "Kargath Bladefist") || (BossName == "Gruul"))
            {
                return 1;
            }
            else if ((BossName == "The Butcher") || (BossName == "Oregorger"))
            {
                return 2;
            }
            else if ((BossName == "Brackenspore") || (BossName == "Blast Furnace"))
            {
                return 3;
            }
            else if ((BossName == "Tectus") || (BossName == "Hans'gar and Franzok"))
            {
                return 4;
            }
            else if ((BossName == "Twin Ogron") || (BossName == "Flamebender Ka'graz"))
            {
                return 5;
            }
            else if ((BossName == "Ko'ragh") || (BossName == "Kromog"))
            {
                return 6;
            }
            else if ((BossName == "Imperator Mar'gok") || (BossName == "Beastlord Darmac"))
            {
                return 7;
            }
            else if (BossName == "Operator Thogar")
            {
                return 8;
            }
            else if (BossName == "Iron Maidens")
            {
                return 9;
            }
            else if (BossName == "Blackhand")
            {
                return 10;
            }
            else
            {
                throw new Exception("Boss not found!");
            }
        }

        public static IEnumerable<Item> GetBossDrops(string bossName, string difficulty) //TODO MAYBE: CHANGE THIS TO LIST OF INT, USE FOREACH TO PARSE THROUGH
        {
            WowDotNetAPI.WowExplorer wowExplorer = new WowDotNetAPI.WowExplorer(WowDotNetAPI.Region.US, WowDotNetAPI.Locale.en_US, GetKey());
            List<Item> itemList = new List<Item>();
            #region Kargath Bladefist
            if (bossName == "Kargath Bladefist")
            {
                itemList.Add(Util.ItemUtil.GetItem(113604, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113591, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113602, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113600, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113601, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113593, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113595, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113596, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113592, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113599, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113598, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113605, difficulty));
            }
            #endregion
            #region The Butcher
            else if (bossName == "The Butcher")
            {
                itemList.Add(Util.ItemUtil.GetItem(113633, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113634, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113636, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113632, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113608, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113610, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113609, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113606, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113611, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113612, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113607, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113637, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113638, difficulty));
            }
            #endregion
            #region Brackenspore
            else if (bossName == "Brackenspore")
            {
                itemList.Add(Util.ItemUtil.GetItem(113660, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113664, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113659, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113661, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113657, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113656, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113655, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113654, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113652, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113658, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113662, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113653, difficulty));
            }
            #endregion
            #region Tectus
            else if (bossName == "Tectus")
            {
                itemList.Add(Util.ItemUtil.GetItem(113639, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113651, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113648, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113649, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113641, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113642, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113647, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113640, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113645, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113644, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113643, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113650, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113646, difficulty));
            }
            #endregion
            #region Twin Ogron
            else if (bossName == "Twin Ogron")
            {
                itemList.Add(Util.ItemUtil.GetItem(113827, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113826, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113667, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113828, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113830, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113666, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113829, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113831, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113835, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113832, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113833, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113834, difficulty));
            }
            #endregion
            #region Ko'ragh
            else if (bossName == "Ko'ragh")
            {
                itemList.Add(Util.ItemUtil.GetItem(113847, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113844, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113845, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113839, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113843, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113846, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113840, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113838, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113837, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113841, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113836, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113842, difficulty));
            }
            #endregion
            #region Imperator
            else if (bossName == "Imperator Mar'gok")
            {
                itemList.Add(Util.ItemUtil.GetItem(113857, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113855, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113848, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113859, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113856, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113850, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113849, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113851, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113853, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113861, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113860, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113858, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113852, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113854, difficulty));
            }
            #endregion
            #region Gruul
            else if (bossName == "Gruul")
            {
                itemList.Add(Util.ItemUtil.GetItem(113872, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113870, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113871, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113869, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(120078, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(118114, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113868, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113863, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113865, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113867, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113864, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113862, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113872, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113866, difficulty));
            }
            #endregion
            #region Oregorger
            else if (bossName == "Oregorger")
            {
                itemList.Add(Util.ItemUtil.GetItem(113877, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113875, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113881, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113876, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113884, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113880, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(119194, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(119448, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(119448, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113874, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113879, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113878, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113883, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113882, difficulty));
            }
            #endregion
            #region Blast Furnace
            else if (bossName == "Blast Furnace")
            {

                itemList.Add(Util.ItemUtil.GetItem(113896, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113895, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113894, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113887, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113888, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113892, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113891, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113886, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113885, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113893, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113889, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113890, difficulty));
                //itemList.Add(Util.ItemUtil.GetItem(119320, difficulty));//TIER PROTECTOR
                //itemList.Add(Util.ItemUtil.GetItem(119313, difficulty));//TIER VANQUISHER
                //itemList.Add(Util.ItemUtil.GetItem(119307, difficulty));//TIER CONQUERER
            }
            #endregion
            #region Hans'gar and Franzok
            else if (bossName == "Hans'gar and Franzok")
            {

                itemList.Add(Util.ItemUtil.GetItem(113906, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113907, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113910, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113904, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113908, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113905, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113899, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113902, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113898, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113903, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113897, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113900, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113901, difficulty));
            }
            #endregion
            #region Flamebender Ka'graz
            else if (bossName == "Flamebender Ka'graz")
            {

                itemList.Add(Util.ItemUtil.GetItem(113921, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113924, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113925, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(120077, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113919, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113917, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113920, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113915, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113914, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113913, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113916, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(119193, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113918, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113923, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113922, difficulty));
                //itemList.Add(Util.ItemUtil.GetItem(119318, difficulty));//TIER CHEST PROTECTOR
                //itemList.Add(Util.ItemUtil.GetItem(119315, difficulty));//TIER CHEST VANQUISHER
                //itemList.Add(Util.ItemUtil.GetItem(119305, difficulty));//TIER CHEST CONQUERER
            }
            #endregion
            #region Kromog
            else if (bossName == "Kromog")
            {
                itemList.Add(Util.ItemUtil.GetItem(113931, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113935, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113929, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113930, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113928, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113934, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113927, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113926, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113937, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113936, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113938, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113933, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113932, difficulty));
                //itemList.Add(Util.ItemUtil.GetItem(119308, difficulty));//TIER HELM CONQUEROR
                //itemList.Add(Util.ItemUtil.GetItem(119321, difficulty));//TIER HELM PROTECTOR
                //itemList.Add(Util.ItemUtil.GetItem(119312, difficulty));//TIER HELM VANQUISHER
            }
            #endregion
            #region Beastlord Darmac
            else if (bossName == "Beastlord Darmac")
            {
                itemList.Add(Util.ItemUtil.GetItem(113950, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113949, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113948, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113951, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113947, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113942, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113943, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113941, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113944, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113939, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113946, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(119192, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113940, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113952, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113945, difficulty));
            }
            #endregion
            #region Operator Thogar
            else if (bossName == "Operator Thogar")
            {
                itemList.Add(Util.ItemUtil.GetItem(113964, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113961, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113962, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113958, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113956, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113960, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113955, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113954, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113953, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113963, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113959, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113957, difficulty));
                //itemList.Add(Util.ItemUtil.GetItem(119314, difficulty));//TIER SHOULDER VANQUISHER
                //itemList.Add(Util.ItemUtil.GetItem(119322, difficulty));//TIER SHOULDER PROTECTOR
                //itemList.Add(Util.ItemUtil.GetItem(119309, difficulty));//TIER SHOULDER CONQUEROR
            } 
            #endregion
            #region Iron Maidens
            else if (bossName == "Iron Maidens")
            {
                itemList.Add(Util.ItemUtil.GetItem(113977, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113968, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113972, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113967, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113971, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113966, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113973, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113965, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113978, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113974, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113970, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113975, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113976, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113969, difficulty));
                //itemList.Add(Util.ItemUtil.GetItem(119319, difficulty));//TIER GLOVES PROTECTOR
                //itemList.Add(Util.ItemUtil.GetItem(119311, difficulty));//TIER GLOVES VANQUISHER
                //itemList.Add(Util.ItemUtil.GetItem(119306, difficulty));//TIER GLOVES CONQUEROR
            } 
            #endregion
            #region Blackhand
            else if (bossName == "Blackhand")
            {
                itemList.Add(Util.ItemUtil.GetItem(113988, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113984, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113990, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113989, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113981, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113980, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113982, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113979, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113985, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113987, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113986, difficulty));
                itemList.Add(Util.ItemUtil.GetItem(113983, difficulty));
                //itemList.Add(Util.ItemUtil.GetItem(119323, difficulty));//TIER ESSENCE PROTECTOR HORDE
                //itemList.Add(Util.ItemUtil.GetItem(119316, difficulty));//TIER ESSENCE VANQUISHER HORDE
                //itemList.Add(Util.ItemUtil.GetItem(120279, difficulty));//TIER ESSENCE PROTECTOR ALLIANCE
                //itemList.Add(Util.ItemUtil.GetItem(120278, difficulty));//TIER ESSENCE VANQUISHER ALLIANCE
                //itemList.Add(Util.ItemUtil.GetItem(119310, difficulty));//TIER ESSENCE CONQUEROR HORDE
                //itemList.Add(Util.ItemUtil.GetItem(120277, difficulty));//TIER ESSENCE CONQUEROR ALLIANCE
            } 
            #endregion
            return itemList;
        }
    }
}
