﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BossPrioritizer.Models
{
    class StatWeightModel
    {
        public Dictionary<string, double> Weights { get; set; }
    }
}
