﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BossPrioritizer.Models
{
    class BossValueFirst : Comparer<WodRaidBoss>
    {
        public override int Compare(WodRaidBoss x, WodRaidBoss y)
        {
            if (x.UpgradeSize != null)
            {
                if (x.Value.CompareTo(y.Value) != 0)
                {
                    return x.Value.CompareTo(y.Value);
                }
                else if (x.UpgradeSize.Count.CompareTo(y.UpgradeSize.Count) != 0)
                {
                    return x.UpgradeSize.Count.CompareTo(y.UpgradeSize.Count);
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
    }
}
