﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BossPrioritizer.Constants;

namespace BossPrioritizer.Models
{
    class UpgradeBossOrder : Comparer<UpgradeModel>
    {
        public override int Compare(UpgradeModel x, UpgradeModel y)
        {
            if (AppConstants.BossNameToInt(x.BossName).CompareTo(AppConstants.BossNameToInt(y.BossName)) != 0)
            {
                return AppConstants.BossNameToInt(x.BossName).CompareTo(AppConstants.BossNameToInt(y.BossName));
            }
            else if (x.UpgradeValue.CompareTo(y.UpgradeValue) != 0)
            {
                return x.UpgradeValue.CompareTo(y.UpgradeValue);
            }
            else 
            {
                return 0;
            }
        }
    }
}
