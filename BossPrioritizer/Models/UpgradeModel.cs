﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowDotNetAPI.Models;

namespace BossPrioritizer.Models
{
    class UpgradeModel
    {
        public string CharacterName { get; set; }
        public string BossName { get; set; }
        public CharacterItem OldItem { get; set; }
        public Item NewItem { get; set; }
        public double UpgradeValue { get; set; }
    }
}
