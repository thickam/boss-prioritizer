﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowDotNetAPI.Models;

namespace BossPrioritizer.Models
{
    public class WodRaidBoss
    {
        public IEnumerable<Item> DropList { get; set; }
        public string Name { get; set; }
        public double Value { 
            get 
            {
                if (UpgradeSize == null)
                {
                    return 0;
                }
                else
                {
                    return UpgradeSize.Sum();
                }
                
            }}
        public List<string> CharactersThatNeedUpgrades { get; set; }
        public List<double> UpgradeSize { get; set; }
        public List<String> NeededUpgrade { get; set; }
    }
}
