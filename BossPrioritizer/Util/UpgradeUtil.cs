﻿using System;
using System.Collections.Generic;
using System.Linq;
using BossPrioritizer.Models;
using WowDotNetAPI;
using WowDotNetAPI.Models;
using BossPrioritizer.Constants;
using System.IO;
using Newtonsoft.Json;

namespace BossPrioritizer.Util
{
    class UpgradeUtil
    {
        public static void UpdateUpgrades(Boolean update, string raid, string difficulty)
	    {
            if (!update)
            {
                Console.WriteLine("Boss comparison not attempted");
            }
            else
	        {
	            List<UpgradeModel> upgrades = new List<UpgradeModel>();
                List<Character> raiders = JsonConvert.DeserializeObject<List<Character>>(File.ReadAllText(AppConstants.BaseDirPath + AppConstants.RaiderListFile));
                List<WodRaidBoss> raidBosses = JsonConvert.DeserializeObject<List<WodRaidBoss>>(File.ReadAllText(AppConstants.BaseDirPath + AppConstants.BossDropFile(raid, difficulty)));
                foreach (Character raider in raiders)
                {
                    //Gets selected spec of raider and converts it to a string for the sake of cleanliness
                    string selectedSpec = raider.Talents.Where(t => t.Selected).First().Spec.Name.ToString();

                    //Gets list of properties of character model attributes
                    var properties = raider.Items.GetType().GetProperties();

                    StatWeightModel statWeights = ItemUtil.GetStatWeightModel(raider, selectedSpec);

                    foreach (var property in properties.Where(a => a.PropertyType.ToString() == "WowDotNetAPI.Models.CharacterItem"))
                    {
                        CharacterItem oldItem = CharacterUtil.GetCharacterItem(raider.Items, property.Name.ToString());
                        //double oldItemValue = ItemUtil.GetItemValue(property.GetValue(), raider.Class.ToString(),selectedSpec);
                        if (property.PropertyType == typeof(CharacterItem))
                        {   //only count it if it is an equippable item attribute
                            foreach (WodRaidBoss boss in raidBosses)
                            {
                                foreach (Item drop in boss.DropList.Where(b => b.InventoryType == ItemUtil.ItemSlotToInventoryType(property.Name)))
                                {
                                    if ((drop.ItemClass == 4) && (drop.ItemSubClass == 0 || drop.ItemSubClass == ItemUtil.CharacterClassToItemSubClass(raider.Class.ToString())))
                                    {
                                        double oldItemValue = ItemUtil.GetItemValue(oldItem.Stats, raider.Class.ToString(), selectedSpec, statWeights);
                                        double newItemValue = ItemUtil.GetItemValue(ItemUtil.GetItemStats(drop), raider.Class.ToString(), selectedSpec, statWeights);
                                        double upgradeValue = newItemValue - oldItemValue;
                                        if (upgradeValue > 0)
                                        {
                                            UpgradeModel upgradeModel = new UpgradeModel();
                                            upgradeModel.BossName = boss.Name.ToString();
                                            upgradeModel.CharacterName = raider.Name;
                                            upgradeModel.NewItem = drop;
                                            upgradeModel.OldItem = oldItem;
                                            upgradeModel.UpgradeValue = upgradeValue;
                                            upgrades.Add(upgradeModel);

                                            if (boss.CharactersThatNeedUpgrades == null)
                                            {
                                                boss.CharactersThatNeedUpgrades = new List<string>();
                                            }
                                            boss.CharactersThatNeedUpgrades.Add(raider.Name);
                                            if (boss.NeededUpgrade == null)
                                            {
                                                boss.NeededUpgrade = new List<string>();
                                            }
                                            boss.NeededUpgrade.Add(drop.Name);
                                            if (boss.UpgradeSize == null)
                                            {
                                                boss.UpgradeSize = new List<double>();
                                            }
                                            boss.UpgradeSize.Add(upgradeValue);
                                            Console.WriteLine("Upgrade Found...");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #region Recording on text file
                //delete file if exists and creates a new file at location specified in AppConstants, opens it for writing
                File.Delete(AppConstants.BaseDirPath + AppConstants.RaiderAuditFile);
                //writes upgrades to text file
                upgrades.Sort(new UpgradeBossOrder());
                foreach (UpgradeModel upgrade in upgrades)
                {
                    string message = upgrade.BossName + " has upgrade " + upgrade.NewItem.Name + " for " + upgrade.CharacterName + " with upgrade value: " + upgrade.UpgradeValue.ToString("0.00") + "\n";
                    Console.Write(message);
                    File.AppendAllText(AppConstants.BaseDirPath + AppConstants.RaiderAuditFile, message);
                }
                foreach (WodRaidBoss boss in raidBosses)
                {
                    Console.Write(boss.Name + " has upgrades for: ");
                    if (boss.CharactersThatNeedUpgrades != null)
                    {
                        foreach (string character in boss.CharactersThatNeedUpgrades)
                        {
                            Console.Write(character + ", ");
                        }
                    }
                    else
                    {
                        Console.Write(" no raiders..");
                    }
                    Console.WriteLine("\b\b.");
                    Console.WriteLine(boss.Name + " has a value of: " + boss.Value.ToString("0.00"));
                }
                raidBosses.Sort(new BossValueFirst());
                foreach (WodRaidBoss boss in raidBosses)
                {
                    Console.WriteLine(boss.Name + ": " + boss.Value.ToString("0.00"));
                }
                #endregion
                Console.WriteLine("Upgrade comparison completed."); 
	        }
	    }
    }
}
