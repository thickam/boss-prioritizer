﻿using BossPrioritizer.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BossPrioritizer.Util
{
    class RaidUtil
    {
        public static void UpdateRaidDrops(Boolean update)
        {
            if (!update)
            {Console.WriteLine("Boss drop files not updated");}
            else 
            {
                foreach (string difficulty in AppConstants.GetDifficulties())
                {
                    foreach (string raid in AppConstants.GetRaidNames())
                    {
                        var serializedData = JsonConvert.SerializeObject(ItemUtil.FillRaidInstance(raid, difficulty));
                        File.Delete(AppConstants.BossDropFile(raid, difficulty));
                        File.AppendAllText(AppConstants.BossDropFile(raid, difficulty), serializedData);
                        Console.WriteLine(AppConstants.BossDropFile(raid, difficulty) + " successfully created.");
                    }
                }
            }
        }
    }
}
