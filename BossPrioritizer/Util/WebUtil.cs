﻿using BossPrioritizer.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BossPrioritizer.Util
{
    class WebUtil
    {
        
        public static void UpdateStatWeights(bool update)
        {
            if (!update)
            {
                Console.WriteLine(AppConstants.StatWeightTextFilePath + " not updated.");
            }
            else 
            {
                using (var client = new WebClient())
                {
                    System.IO.File.Delete(AppConstants.BaseDirPath + AppConstants.StatWeightTextFilePath);
                    System.IO.File.AppendAllText(AppConstants.BaseDirPath + AppConstants.StatWeightTextFilePath, client.DownloadString(AppConstants.AMRWeightAddress));
                    Console.WriteLine(AppConstants.StatWeightTextFilePath + " successfully updated.");
                }
            }
        }
    }
}
