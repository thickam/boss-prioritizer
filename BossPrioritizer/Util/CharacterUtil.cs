﻿using BossPrioritizer.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using WowDotNetAPI;
using WowDotNetAPI.Models;
using System.Web.Script.Serialization;
using System.IO;
using Newtonsoft.Json;

namespace BossPrioritizer.Util
{
    class CharacterUtil
    {
        public static CharacterItem GetCharacterItem(CharacterEquipment equipment, string name)
        {
            if (name == "Back")
            {
                return equipment.Back;
            }
            else if (name == "Chest")
            {
                return equipment.Chest;
            }
            else if (name == "Feet")
            {
                return equipment.Feet;
            }
            else if (name == "Finger1")
            {
                return equipment.Finger1;
            }
            else if (name == "Finger2")
            {
                return equipment.Finger2;
            }
            else if (name == "Hands")
            {
                return equipment.Hands;
            }
            else if (name == "Head")
            {
                return equipment.Head;
            }
            else if (name == "Legs")
            {
                return equipment.Legs;
            }
            else if (name == "MainHand")
            {
                return equipment.MainHand;
            }
            else if (name == "Neck")
            {
                return equipment.Neck;
            }
            else if (name == "OffHand")
            {
                return equipment.OffHand;
            }
            else if (name == "Ranged")
            {
                return equipment.Ranged;
            }
            else if (name == "Shirt")
            {
                return equipment.Shirt;
            }
            else if (name == "Shoulder")
            {
                return equipment.Shoulder;
            }
            else if (name == "Trinket1")
            {
                return equipment.Trinket1;
            }
            else if (name == "Trinket2")
            {
                return equipment.Trinket2;
            }
            else if (name == "Waist")
            {
                return equipment.Waist;
            }
            else if (name == "Wrist")
            {
                return equipment.Wrist;
            }
            else
            {
                throw new Exception("Unknown character item type");
            }
        }

        public static void UpdateRaiders(Boolean update)
        {
            if (!update)
            {
                Console.WriteLine(AppConstants.RaiderListFile + " not updated.");
            }
            else
	        {
	            Console.WriteLine("Characters included in update:");
                WowExplorer wowexplorer = new WowExplorer(Region.US, Locale.en_US, AppConstants.GetKey());
                List<Character> raiders = new List<Character>();
                foreach(string raider in AppConstants.GetRaiders())
                {
                    Character character = wowexplorer.GetCharacter(raider.Split('-').ElementAt(1), raider.Split('-').ElementAt(0), CharacterOptions.GetItems);
                    character.Talents = wowexplorer.GetCharacter(raider.Split('-').ElementAt(1), raider.Split('-').ElementAt(0), CharacterOptions.GetTalents).Talents;
                    raiders.Add(character);
                    Console.WriteLine(character.Name.ToString());
                }
                var serializedData = JsonConvert.SerializeObject(raiders);
                File.Delete(AppConstants.BaseDirPath + AppConstants.RaiderListFile);
                File.AppendAllText(AppConstants.BaseDirPath + AppConstants.RaiderListFile, serializedData);
                Console.WriteLine(AppConstants.RaiderListFile + " successfully updated."); 
	        }
        }
    }
}
