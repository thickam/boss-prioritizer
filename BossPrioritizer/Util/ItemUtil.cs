﻿using BossPrioritizer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowDotNetAPI.Models;
using System.Net;
using WowDotNetAPI.Utilities;
using System.IO;
using WowDotNetAPI;
using BossPrioritizer.Constants;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace BossPrioritizer.Util
{
    class ItemUtil
    {
        static string UppercaseFirst(string s) //Source: http://www.dotnetperls.com/uppercase-first-letter
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static StatWeightModel GetStatWeightModel(Character character, string selectedSpec)
        {
            selectedSpec = Regex.Replace(selectedSpec, @"\s+", "");
            StatWeightModel statWeightModel = new StatWeightModel();
            statWeightModel.Weights = new Dictionary<string, double>();
            string jsonToParse = File.ReadAllText(AppConstants.BaseDirPath + AppConstants.StatWeightTextFilePath);
            string[] jsonObjects = jsonToParse.Split(new string[] { "}}]," }, System.StringSplitOptions.RemoveEmptyEntries);
            string stringToParse = jsonObjects.Where(p => p.Contains(UppercaseFirst(character.Class.ToString().ToLower()))).Where(p => p.Contains(selectedSpec)).First();
            string[] statWeightIds = stringToParse.Split(new string[] { "},{" }, System.StringSplitOptions.RemoveEmptyEntries);
            string statWeights = statWeightIds.First().Split(new string[] { "}," }, System.StringSplitOptions.RemoveEmptyEntries).Where(p => p.Contains("Weights")).First().Split(new string[] {"Weights"}, System.StringSplitOptions.RemoveEmptyEntries).ElementAt(1);
            string[] weights = statWeights.Split((new char[] {','}));
            for (int i = 0; i < weights.Length; i++ )
            {
                List<string> singleWeight = weights.ElementAt(i).Split(new char[] { '"' }, System.StringSplitOptions.RemoveEmptyEntries).ToList();
                if (i == 0)
                {
                    singleWeight.Remove(singleWeight.ElementAt(0));
                }
                for (int o = 0; o < singleWeight.Count; o++)
                {
                    singleWeight[o] = singleWeight[o].Trim(new char[] { '"', '\\', ':' });
                    singleWeight[o] = singleWeight[o].TrimEnd(new char[] { '"', '\\', ':' });
                }
                statWeightModel.Weights.Add(singleWeight[0], Convert.ToDouble(singleWeight[1]));
            }
            return statWeightModel;
        }

        public static List<ItemStat> GetItemStats(Item item)
        {
            List<ItemStat> itemStats = new List<ItemStat>();
            foreach (ItemBonusStat itemBonusStat in item.BonusStats)
            {
                ItemStat itemStat = new ItemStat();
                itemStat.Amount = itemBonusStat.Amount;
                itemStat.Stat = itemBonusStat.Stat;
                itemStat.Reforged = itemBonusStat.Reforged;
                itemStats.Add(itemStat);
            }
            return itemStats;
        }

        public static int ItemSlotToInventoryType(string slot)
        {
            if (slot == "Head")
            {
                return 1;
            }
            else if (slot == "Neck")
            {
                return 2;
            }
            else if (slot == "Shoulder")
            {
                return 3;
            }
            else if (slot == "Shirt")
            {
                return 4;
            }
            else if (slot == "Chest")
            {
                return 5;
            }
            else if (slot == "Waist")
            {
                return 6;
            }
            else if (slot == "Legs")
            {
                return 7;
            }
            else if (slot == "Feet")
            {
                return 8;
            }
            else if (slot == "Wrist")
            {
                return 9;
            }
            else if (slot == "Hands")
            {
                return 10;
            }
            else if ((slot == "Finger1") || (slot == "finger2"))
            {
                return 11;
            }
            else if ((slot == "Trinket1") || (slot == "Trinket2"))
            {
                return 120;
            }
            else if (slot == "One-Hand")
            {
                return 13;
            }
            else return 1000000;
        }

        public static Item GetItem(int id, string difficulty)
        {
            Item item;

            TryGetData<Item>(
                string.Format(@"{0}/wow/item/{1}/{4}?locale={2}&apikey={3}", "https://us.api.battle.net", id, Locale.en_US, AppConstants.GetKey(), "raid-" + difficulty),
                out item);

            return item;
        }

        public static int CharacterClassToItemSubClass (string characterClass)
        {
            if ((characterClass == "MAGE") || (characterClass =="PRIEST") || (characterClass == "WARLOCK"))
            {
                return 1;
            }
            else if ((characterClass == "DRUID") || (characterClass == "MONK") || (characterClass == "ROGUE"))
            {
                return 2;
            }
            else if ((characterClass == "HUNTER") || (characterClass == "SHAMAN"))
            {
                return 3;
            }
            else if ((characterClass == "DEATHKNIGHT") || (characterClass =="PALADIN") || (characterClass == "WARRIOR"))
            {
                return 4;
            }
            else if ((characterClass == "SHAMAN") || (characterClass =="PALADIN") || (characterClass == "WARRIOR"))
            {
                return 6;
            }
            else 
            {
                throw new Exception("invalid item subclass");
            }
        }

        private static void TryGetData<T>(string url, out T requestedObject) where T : class
        {
            try
            {
                requestedObject = JsonUtility.FromJSON<T>(url);
            }
            catch (Exception ex)
            {
                requestedObject = null;
                throw ex;
            }
        }

        public static List<WodRaidBoss> FillRaidInstance(string raid, string difficulty)
        {
            List<WodRaidBoss> raidBosses = new List<WodRaidBoss>();
            foreach (string bossName in Constants.AppConstants.GetBossNames(raid))
            {
                WodRaidBoss wodRaidBoss = new WodRaidBoss();
                wodRaidBoss.Name = bossName;
                wodRaidBoss.DropList = new List<Item>(Constants.AppConstants.GetBossDrops(wodRaidBoss.Name, difficulty));
                raidBosses.Add(wodRaidBoss);
            }
            return raidBosses;
        }

        public static double GetItemValue(IEnumerable<ItemStat> enumerable, string characterClass, string characterSpec, StatWeightModel statWeightModel)
        {
            double valueTracker = new double();
            foreach(ItemStat stat in enumerable)
            {
                valueTracker += stat.Amount * GetStatWeight(statWeightModel, GetStatName(Convert.ToInt32(stat.Stat)));
            }
            return valueTracker;
        }

        private static double GetStatWeight(StatWeightModel statWeightModel, string statName)
        {
            if (statWeightModel.Weights.Keys.Contains(statName))
            {
                return statWeightModel.Weights[statName];
            }
            else
            {
                return 0.0;
            }
        }
        
        private static string GetStatName(int itemStat)
        {
            if (itemStat == 3) //AGILITY
            {
                return "Agility";
            }
            else if (itemStat == 4) //STRENGTH
            {
                return "Strength";
            }
            else if (itemStat == 5) //INTELLECT
            {
                return "Intellect";
            }
            else if (itemStat == 6)//SPIRIT
            {
                return "Spirit";
            }
            else if (itemStat == 7)//STAMINA
            {
                return "Stamina";
            }
            else if (itemStat == 32)//CRIT STRIKE
            {
                return "CriticalStrike";
            }
            else if (itemStat == 36)//HASTE
            {
                return "Haste";
            }
            else if (itemStat == 40)//VERSATILITY
            {
                return "Versatility";
            }
            else if (itemStat == 45)//SPELL POWER
            {
                return "SpellPower";
            }
            else if (itemStat == 49)//MASTERY
            {
                return "Mastery";
            }
            else if (itemStat == 50)//BONUS ARMOR
            {
                return "BonusArmor";
            }
            else if (itemStat == 59)//MULTISTRIKE
            {
                return "Multistrike";
            }
            else if(itemStat == 61)//Movement Speed
            {
                return "MovementSpeed";
            }
            else if (itemStat == 62)//LEECH
            {
                return "Leech";
            }
            else if (itemStat == 63)//AVOIDANCE
            {
                return "Avoidance";
            }
            else if (itemStat == 64)//indestructable??
            {
                return "Indestructable";
            }
            else if (itemStat == 72)//STRENGTH
            {
                return "Strength";
            }
            else if (itemStat == 73)//AGILITY
            {
                return "Agility";
            }
            else if (itemStat == 74)//STRENGTH
            {
                return "Strength";
            }
            else
            {
                return "StatNotFound";
            }
        }

    }
}
