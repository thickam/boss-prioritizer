﻿using BossPrioritizer.Constants;
using BossPrioritizer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using WowDotNetAPI;
using WowDotNetAPI.Models;
using BossPrioritizer.Models;
using System.IO;

namespace BossPrioritizer
{
    static class Program
    {
        static void Main()
        {
            //Pull down stat weights
            WebUtil.UpdateStatWeights(false); //updates the stat weights, pulling latest from askmrrobot.com

            //Pull down list of raiders
            CharacterUtil.UpdateRaiders(false); //Pulls characters down from bnet, serializes, and stores in RaiderList.txt resource file

            //populate list of bosses
            RaidUtil.UpdateRaidDrops(false); //Creates files for each raid in format raid-difficulty.txt (e.g. "Highmaul-heroic.txt"

            //parse through raiders to find upgrades
            UpgradeUtil.UpdateUpgrades(true, "Highmaul", "heroic"); //TODO: make this sort properly && fix class not being recorded/retrieved

            Console.Write("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
